import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UniversalValidators} from 'ngx-validators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit, OnDestroy {
  activeField: number[][] = [];
  fieldLength = 12;
  isGenerating = false;
  nextIter = new Subject<boolean>();
  sub: Subscription;
  DELAY = 100;
  RATIO = 50;
  form: FormGroup;
  generationCounter = 0;
  aliveCells = 0;
  lastStates = [0, 1, 2, 3, 4];
  isEnd = false;

  private getRandomInt(max): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  private getRandomIntWithRatio(ratio): number {
    return this.getRandomInt(100) <= ratio - 1 ? 1 : 0;
  }

  sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  hash(s: string) {
    let hash = 0;
    const str = String(s);
    if (str.length === 0) {
      return hash;
    }
    for (let i = 0; i < str.length; i++) {
      const char = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + char;
      hash = hash & hash;
    }
    return hash;
  }

  private randomlyFillArray() {
    this.generationCounter = 0;
    this.aliveCells = 0;

    for (let x = 0; x < this.fieldLength; x++) {
      this.activeField[x] = [];
    }

    for (const x of [0, this.fieldLength - 1]) {
      for (let y = 0; y < this.fieldLength - 1; y++) {
        this.activeField[x][y] = 8;
        this.activeField[y][x] = 8;
      }
    }

    for (let x = 1; x < this.fieldLength - 1; x++) {
      for (let y = 1; y < this.fieldLength - 1; y++) {
        this.activeField[x][y] = this.getRandomIntWithRatio(this.RATIO);
        if (this.activeField[x][y] === 1) {
          this.aliveCells += 1;
        }
      }
    }
  }

  play() {
    console.log('Starting');
    this.isGenerating = true;
    this.processField();
    this.doProcessing();
  }

  pause() {
    console.log('Pausing');
    this.isGenerating = false;
  }

  restart() {
    this.isEnd = false;
    this.lastStates = [0, 1, 2, 3, 4];
    this.pause();
    this.randomlyFillArray();
  }

  ngOnInit(): void {
    this.randomlyFillArray();
    this.form = new FormGroup({
      delay: new FormControl(null, [Validators.required, UniversalValidators.min(10)]),
      length: new FormControl(null, [Validators.required, UniversalValidators.min(3), UniversalValidators.max(30)]),
      ratio: new FormControl(null, [Validators.required, UniversalValidators.min(5), UniversalValidators.max(80)]),
    });
    const sub = this.nextIter.asObservable();
    this.sub = sub.subscribe(
      () => {
        this.doProcessing();
      }
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  private processField() {
    console.log('Generating');
    const arraycl = Object.assign({}, this.activeField);
    let zeroCount = 0;
    let oneCount = 0;
    for (let i = 0; i < this.fieldLength; i++) {
      for (let j = 0; j < this.fieldLength; j++) {
        if (this.activeField[i][j] === 0) {
          zeroCount = zeroCount + 1;
        } else if (this.activeField[i][j] === 1) {
          oneCount = oneCount + 1;
        }
        if ((i > 0 && i < this.fieldLength - 1) && (j > 0 && j < this.fieldLength - 1)) {

          let liveNeighbours = 0;
          if (this.activeField[i][j] === 0) {
            if (this.activeField[i - 1][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i - 1][j] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i - 1][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }


            if (liveNeighbours === 3) {
              arraycl[i][j] = 1;
            } else {
              arraycl[i][j] = 0;
            }

          } else if (this.activeField[i][j] === 1) {

            if (this.activeField[i - 1][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i - 1][j] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i - 1][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j - 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }

            if (this.activeField[i + 1][j + 1] === 1) {
              liveNeighbours = liveNeighbours + 1;
            }


            if (!(liveNeighbours > 1) || !(liveNeighbours < 4)) {
              arraycl[i][j] = 0;
            }
          }
        }
      }
    }
    const old = this.lastStates[0];
    let i = 0;
    for (const state of this.lastStates.slice(1)) {
      if (old === state) {
        i += 1;
      } else {
        break;
      }
    }
    if (i === 4 || zeroCount === (this.fieldLength - 2) * (this.fieldLength - 2)) {
      this.isGenerating = false;
      this.isEnd = true;
    } else {
      for (let i = 0; i < this.lastStates.length - 1; i++) {
        this.lastStates[i] = this.lastStates[i + 1];
      }
      this.lastStates[4] = this.hash(this.activeField.toString());
    }
    this.generationCounter += 1;
    this.aliveCells = oneCount;
  }

  private doProcessing() {
    this.sleep(this.DELAY).then(() => {
      if (this.isGenerating) {
        this.processField();
        this.nextIter.next(true);
      }
    });
  }

  save() {
    this.pause();
    this.DELAY = Number(this.form.controls.delay.value);
    this.fieldLength = Number(this.form.controls.length.value) + 2;
    this.RATIO = Number(this.form.controls.ratio.value);
    this.activeField = [];
    this.randomlyFillArray();
    this.isEnd = false;
    this.form.reset();
  }
}
